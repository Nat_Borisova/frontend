"use strict";

/*Класс с методами для работы с таблицей*/
class TableManager {

    /* Функция добавления строки в таблицу*/
    static addRow(table) {

        let currentRow = table.insertRow(-1);

        let nameBox = currentRow.insertCell(0);

        let valueBox = currentRow.insertCell(1);

        let dateBox = currentRow.insertCell(2);

        let deleteRowBox = currentRow.insertCell(3);
        deleteRowBox.innerHTML = '<button type="button">Удалить строку</button>';
        deleteRowBox.addEventListener("click", function () {
            TableCookiesManager.Delete(this)
        });

        table.appendChild(currentRow);

        return currentRow;
    };

    /* Функция заполнения строки значениями*/
    static fillRow(currentRow, name, value, date = "") {

        currentRow.cells[0].innerHTML = name;
        currentRow.cells[1].innerHTML = value;
        currentRow.cells[2].innerHTML = date;

    };

    /* Функция удаления строки из таблицы*/
    static deleteRow(table, rowIndex) {

        table.deleteRow(rowIndex);

    };

    /* Функция поиска строки в таблице*/
    static findRow(table, name) {

        for (let i = 1; i < table.rows.length; i++) {
            if (table.rows[i].cells[0].innerHTML.replace(/\s+/g, '') === name.replace(/\s+/g, '')) {
                return table.rows[i];
            }
        }

        return undefined;
    };

}

/* Класс для работы с cookies*/
class CookieManager {

    /* Функция добавления cookie к существующим*/
    static setCookie(name, value, options = {}) {

        options = {
            path: '/',
            ...options
        };

        if (options.expires && options.expires.toUTCString) {
            options.expires = options.expires.toUTCString();
        }

        let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

        for (let optionKey in options) {
            updatedCookie += "; " + optionKey;
            let optionValue = options[optionKey];
            if (optionValue !== true) {
                updatedCookie += "=" + optionValue;
            }
        }

        document.cookie = updatedCookie;
    };

    /* Функция удаления cookie по имени*/
    static deleteCookie(name) {
        this.setCookie(name, "", {
            'max-age': -1
        })
    }
}

/* Основной класс для работы с программой*/
class TableCookiesManager {

    /* Функция заполнения таблицы существующими cookies*/
    static fillTable() {

        document.cookie.split(";").forEach(function (cookie) {

            let fields = cookie.split("=");

            let name = fields[0];
            let value = fields[1];

            let tableCookies = document.getElementById("tableCookies");
            if (name && value) {
                let row = TableManager.addRow(tableCookies, name, value);
                TableManager.fillRow(row, name, value);
            }
        });

    }

    /* Функция добавления cookies и обновления таблицы*/
    static Add() {

        let name = document.querySelector('#cookieName').value;
        let value = document.querySelector('#cookieValue').value;
        let date = document.querySelector('#cookieDate').value;

        if (!name || !value || !date) {
            alert(`Для добавления строки необходимо заполнить следующие поля: ${!name ? "Имя, " : ""}${!value ? "Значение, " : ""}${!date ? "Дата" : ""}`);
            return;
        }

        if (new Date() > new Date(date)) {
            alert(`Дата cookie должна быть больше текущей даты`);
            return;
        }

        CookieManager.setCookie(name, value, {'expires': new Date(date)});

        let tableCookies = document.getElementById("tableCookies");

        let findedRow = TableManager.findRow(tableCookies, name);

        if (findedRow) {
            TableManager.fillRow(findedRow, name, value, date)
        } else {
            let addedRow = TableManager.addRow(tableCookies, name, value, date);
            TableManager.fillRow(addedRow, name, value, date);
        }

    };


    /* Функция удаления cookies и обновления таблицы*/
    static Delete(currentElem) {

        let name = currentElem.parentNode.getElementsByTagName("td")[0].innerHTML;

        let tableCookies = document.getElementById("tableCookies");

        CookieManager.deleteCookie(name.replace(/\s+/g, ''));
        TableManager.deleteRow(tableCookies, currentElem.parentNode.rowIndex);

    }
}

