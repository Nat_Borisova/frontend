import * as ArticlesModel from './articles';
import 'babel-polyfill';

async function articlesManager() {

    let allArticles = await ArticlesModel.all();
    console.log('articles count = ' + allArticles.length);

    // берём случайный индекс
    let ind = Math.floor(Math.random() * allArticles.length);
    console.log('select index ' + ind + ', id = ' + allArticles[ind].id);

    // получаем статью по id
    let oneArticle = await ArticlesModel.one(allArticles[ind].id);
    console.log(oneArticle);

    // пробуем удалить её
    let removedArticle = await ArticlesModel.remove(oneArticle.id);
    console.log('что с удалением? - ' + removedArticle);

    // а сколько статей в базе сейчас
    let resultArticles = await ArticlesModel.all();
    console.log('articles count = ' + resultArticles.length);
}

articlesManager().then().catch((error) => {
    console.log(error)
});