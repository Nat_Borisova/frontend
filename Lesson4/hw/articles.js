import * as serverApi from './db';

async function all() {
    let response = await serverApi.all();
    return ServerResponse(response);
}

async function one(id) {
    let response = await serverApi.get(id);
    return ServerResponse(response);
}

async function remove(id) {
    let response = await serverApi.remove(id);
    return ServerResponse(response);
}

function ServerResponse(response) {
    try {
        let info = JSON.parse(response);

        if (info.code === 200) {
            return info.data;
        }
        else {
            return info.status;
        }
    }
    catch (e) {
        throw new Error(e);
    }
}

export { all, one, remove };