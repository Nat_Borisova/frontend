import {tns} from "./node_modules/tiny-slider/src/tiny-slider"

document.addEventListener("DOMContentLoaded", () => {
    generator.next();
});

let sliderData = [
    {
        image: "https://ptzgovorit.ru/sites/default/files/original_nodes/stikhotvorenie-zolotaya-osen.-agniya-barto.jpg",
        title: 'Осень'
    },
    {
        image: "https://www.lybach.com/file/pic/photo/2018/02/ea761f150ac281468055a3e75ef4d7ce_1024.jpg",
        title: 'Зима'
    },
    {
        image: "https://dok7xy59qfw9h.cloudfront.net/40a/bf375/f53e/4c0f/906b/72025129ec85/large/52834.jpg",
        title: 'Весна'
    },
    {
        image: "https://www.nastol.com.ua/pic/201404/1024x768/nastol.com.ua-92859.jpg",
        title: 'Лето'
    }
];

const slider = tns({
    container: '.my-slider',
    controlsContainer: "#customize-controls",
    loop: true,
    items: 1,
    slideBy: 'page',
    nav: false,
    autoplay: true,
    speed: 400,
    autoplayButtonOutput: false,
    lazyload: true,
    center: true
});

let generator = generateSlide();

function* generateSlide() {
    
    while (true) {

        let slideIndex = slider.getInfo().index;
        
        let slide = document.getElementsByClassName('slider-item')[slideIndex];

        createAndFillHeader(slide, slideIndex); // добавление и заполнение заголовка данными из sliderData по индексу
        createAndFillImage(slide, slideIndex);  // добавление и заполнение изображения данными из sliderData по индексу
        
        yield true;
    }
}

function createAndFillHeader(slide, slideIndex){

    let header = document.createElement("H1");
    header.id = `slider-head${slideIndex}`;
    let title = document.createTextNode(sliderData[slideIndex - 1].title);
    header.appendChild(title);
    slide.appendChild(header);
    
}

function createAndFillImage(slide, slideIndex){
   
    let image = document.createElement("img");
    image.id = `slider-img${slideIndex}`;
    image.src = sliderData[slideIndex - 1].image;
    image.srcset = sliderData[slideIndex - 1].image;
    slide.appendChild(image);
    
}

let switchSlide = function (info, eventName) {
    if (info.index <= sliderData.length && info.index > 0)
        generator.next();
};

let removePrevSlide = function (info, eventName) {
    let slide = document.getElementsByClassName('slider-item')[info.indexCached];
    slide.innerHTML = "";
};

slider.events.on('indexChanged', switchSlide);
slider.events.on('transitionEnd', removePrevSlide);
