let path = require('path');
let conf = {
    entry: './slider.js',
    output: {
        path: path.resolve(__dirname, './js'), // в идеале полный
        filename: 'bundle.js',
        publicPath: 'js/'
    },
    devServer: {
        overlay: true
    }
};
module.exports = conf;