let SqrCalc = require('./module/');

let myCalculator = new SqrCalc(100);
console.log(myCalculator.sum(4, 6, 10)); //вернет 10 404 (100 + 2 = 102 * 102)
console.log(myCalculator.dif(10, 5)); //вернет 8 464 (102 - 10 = 92 * 92)
console.log(myCalculator.div(5, 3)); //вернет 2 116 (92 / 2 = 46 * 46)
console.log(myCalculator.mul(2, 10)); //вернет 8 464 (46 * 2 = 92 * 92)