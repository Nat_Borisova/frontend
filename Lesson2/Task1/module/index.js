
class BaseCalc {

    result = 0;

    constructor(value) {
        this.result = value;
    }
    
    sum(...values) {
        if (this.isInputDataCorrect(...values)) {
            for (let i = 0; i < values.length; i++) {
                this.result += values[i];
            }
        }
        return this.result;
    }

    dif(...values) {
        if (this.isInputDataCorrect(...values)) {
            for (let i = 0; i < values.length; i++) {
                this.result -= values[i];
            }
        }
        return this.result;
    }

    div(...values) {
        if (this.isInputDataCorrect(...values)) {
            for (let i = 0; i < values.length; i++) {
                if (values[i] === 0) continue;
                this.result /= values[i];
            }
        }
        return this.result;
    }

    mul(...values) {
        if (this.isInputDataCorrect(...values)) {
            for (let i = 0; i < values.length; i++) {
                this.result *= values[i];
            }
        }
        return this.result;
    }
    
    isInputDataCorrect(...values){
        return values && values.length;
    }
}

class SqrCalc extends BaseCalc{
    
    constructor(value) {
        super(value);
    }

    sum(...values) {
        return this.sqr(super.sum(...values));
    }

    dif(...values) {
        return this.sqr(super.dif(...values));

    }
    
    div(...values) {
        return this.sqr(super.div(...values));

    }

    mul(...values) {
        return this.sqr(super.mul(...values));

    }

    sqr(value) {
        return Math.pow(value, 2);
    }
}

module.exports = SqrCalc;