function isEqualDeep(obj1, obj2) {

    let Properties1 = Object.keys(obj1);
    let Properties2 = Object.keys(obj2);

    if (Properties1.length !== Properties2.length)
        return false;

    let result = true;

    for (let i = 0; i < Properties1.length; i++) {

        let Prop = Properties1[i];

        if (!obj2.hasOwnProperty(Prop)) {
            
            result = false;
            break;
            
        }

        let Value1 = obj1[Prop];
        let Value2 = obj2[Prop];

        if (typeof Value1 === "object") {

            if (Array.isArray(Value1)) {

                if (!isEqualArrays(Value1, Value2)) {
                    
                    result = false;
                    break;
                    
                }

            } else if (!isEqualDeep(Value1, Value2)) {
                
                result = false;
                break;
                
            }
            
            continue;
        }

        if (Value1 !== Value2) {
            
            result = false;
            break;
            
        }

    }

    return result;

}

function isEqualArrays(arr1, arr2) {

    for (let j = 0; j < arr1.length; j++) {
        if (arr1[j] !== arr2[j])
            return false;
    }

    return true;
}

module.exports = isEqualDeep;