function mySplice(array, start = 0, deleteCount, ...items) {

    if (!inputDataIsCorrect(array, start, deleteCount, ...items)) return;

    console.log('Введенный массив:\n' + array);

    let arrSize = array.length;

    if (start > arrSize)
        start = arrSize - 1;
    else if (start < 0)
        start = arrSize + start;
    //если (array, start), то удалить с начального индекса
    if (typeof deleteCount == "undefined" && items.length === 0)
        deleteCount = array.length - start;

    if (deleteCount > arrSize - start) {
        array.length = 0;
        return;
    }

    let newArr = [];
    let j = 0;

    for (let i = 0; i < arrSize; i++) {
        if (i === start && items.length > 0) {
            for (let k = 0; k < items.length; k++) {
                newArr[j++] = items[k]; //вставляем items
            }
            if (deleteCount === 0) {
                newArr[j++] = array[i]; //добавляем исходные, если ничего не нужно удалять
            }
        } else if (i < start || i > start + deleteCount - 1) {
            newArr[j++] = array[i];     //добаляем оставшиеся элементы не из интервала [start, deletecount]
        }
    }
    array = newArr;

    console.log('Измененный массив:\n' + array);

}

/**
 * Проверяет входные параметры
 * @return {boolean}
 */
function inputDataIsCorrect(array, start, deleteCount, ...items) {

    let result = true;

    if (!Array.isArray(array)) {
        console.log('Необходимо ввести массив!');
        result = false;
    }

    if (typeof start != "number") {
        console.log('Стартовый индекс должен быть числом!');
        result = false;
    }

    if (typeof deleteCount != "number" && typeof deleteCount != "undefined" || deleteCount < 0) {
        console.log('Количество удаляемых элементов должно быть неотрицательным(<=0) числом!');
        result = false;
    }

    if (deleteCount === 0 && items.length === 0) {
        console.log('Если количество удаляемых элементов = 0, необходимо ввести добавляемые элементы!');
        result = false;
    }

    return result;

}

module.exports = mySplice;