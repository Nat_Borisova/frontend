function consolePrint(array, count = 0) {

    if (!Array.isArray(array)) {
        console.log('Необходимо ввести массив!');
        return;
    }

    if (count !== array.length) {
        console.log(array[count]);
        consolePrint(array, count + 1);
    }

}

module.exports = consolePrint;